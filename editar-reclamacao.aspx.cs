﻿using ReclamaPoa2013.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ReclamaPoa2013
{
    public partial class editar_reclamacao : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                String filtro = (string)Request.QueryString["id"];
                int id;

                if (Int32.TryParse(filtro, out id))
                {

                    ReclamaPoaEntities _db = new ReclamaPoaEntities();

                    //Reclamacao rec = new Reclamacao();
                    //IQueryable<Reclamacao> query = _db.Reclamacoes.Where(r => r.ReclamacaoId == id);
                    //var rec = query.First();

                    var query = from reclamacao in _db.Reclamacoes
                                where reclamacao.ReclamacaoId == id
                                select new ReclamacaoViewModel()
                                {
                                    Titulo = reclamacao.Titulo,
                                    Descricao = reclamacao.Descricao,
                                    Data = reclamacao.Data,
                                    Endereco = reclamacao.Endereco,
                                    Situacao = reclamacao.Situacao.ToString(),
                                    Bairro = reclamacao.Bairro.Nome,
                                    Categoria = reclamacao.Categoria.Nome,
                                    UrlImagem = reclamacao.UrlImagem,
                                    Usuario = reclamacao.Usuario
                                };
                    var rec = query.First();

                    txtNomeRec.Text = rec.Titulo;
                    txtDescRec.Text = rec.Descricao;
                    txtEndereco.Text = rec.Endereco;
                    img1.ImageUrl = rec.UrlImagem;
                    txtUrlImagem.Text = rec.UrlImagem;
                    lblBairro.Text = rec.Bairro;
                    lblCategoria.Text = rec.Categoria;
                    lblData.Text = rec.Data.ToShortDateString();
                }
            }
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            String filtro = (string)Request.QueryString["id"];
            int id = Int32.Parse(filtro);
            
            ReclamaPoaEntities _db = new ReclamaPoaEntities();

            Reclamacao reclamacao = new Reclamacao();

            reclamacao = _db.Reclamacoes.Where(r => r.ReclamacaoId == id).First();

            reclamacao.Titulo = txtNomeRec.Text;
            reclamacao.Descricao = txtDescRec.Text;

            string nomeBairro = lblBairro.Text;
            Bairro bairro = (from b in _db.Bairros
                             where b.Nome == nomeBairro
                             select b).First();
            reclamacao.Bairro = bairro;

            string nomeCat = lblCategoria.Text;
            Categoria cat = (from c in _db.Categorias
                             where c.Nome == nomeCat
                             select c).First();
            reclamacao.Categoria = cat;

            if (cbResolvida.Checked)
            {
                reclamacao.Situacao = Situacao.Resolvida;
            }

            reclamacao.Endereco = txtEndereco.Text;
            reclamacao.UrlImagem = txtUrlImagem.Text;
            reclamacao.Usuario = Context.User.Identity.Name;

            _db.SaveChanges();

            Response.Redirect("Detalhes.aspx?id=" + id);

        }
    }
}


///



///