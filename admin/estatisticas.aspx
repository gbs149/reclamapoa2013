﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="estatisticas.aspx.cs" Inherits="ReclamaPoa2013.painel_de_controle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h1>Estatísticas</h1>

        <p>Total de reclamações:</p>
        <p>
            <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label></p>
        <p>Total de Comentários:</p>
        <p>
            <asp:Label ID="lblTotalComentarios" runat="server" Text="Label"></asp:Label></p>
        <p>Média de
            <asp:Label ID="lblMediaComentGeral" runat="server" Text="Label"></asp:Label>
            comentários por reclamação.</p>

    </div>


    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div class="jumbotron" style="min-height:40rem">
            <div class="col-sm-4">
                <p>Filtrar por Categoria</p>
                <asp:DropDownList CssClass="form-control" ID="ddlCategorias" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCategorias_SelectedIndexChanged"></asp:DropDownList>
            </div>

            <div class="col-sm-4">
                <p>Filtrar por Bairro</p>
                <asp:DropDownList CssClass="form-control" ID="ddlBairros" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBairros_SelectedIndexChanged"></asp:DropDownList>
            </div>

            <div class="col-sm-4">
                <p>Filtrar por período</p>
                <p>
                    Data inicial:
                <asp:TextBox CssClass="form-control" ID="txtDataInicio" runat="server" AutoPostBack="true" OnTextChanged="txtDataInicio_TextChanged"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender Format="dd/MM/yyyy" TargetControlID="txtDataInicio" ID="CalendarExtender1" runat="server" />
                </p>

                <p>
                    Data final: 
                <asp:TextBox CssClass="form-control" ID="txtDataFinal" runat="server" AutoPostBack="true" OnTextChanged="txtDataFinal_TextChanged"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender Format="dd/MM/yyyy" TargetControlID="txtDataFinal" ID="CalendarExtender2" runat="server" />
                </p>
            </div>

            
                <div>
                    <p>
                        <asp:Label ID="lblSubTotal" runat="server" Text=""></asp:Label></p>
                </div>
                <div>
                    <p>
                        <asp:Label ID="lblMediaFiltrada" runat="server" Text=""></asp:Label></p>
                </div>
                <div>
                    <p>
                        <asp:Label ID="lblAbertas" runat="server" Text=""></asp:Label></p>
                    <p>
                        <asp:Label ID="lblResolvidas" runat="server" Text=""></asp:Label></p>
                    <p>
                        <asp:Label ID="lblEncerradas" runat="server" Text=""></asp:Label></p>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
