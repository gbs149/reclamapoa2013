﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="editar-usuario.aspx.cs" Inherits="ReclamaPoa2013.admin.editar_usuario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div>

        <div class="form-group">
            <asp:Label ID="Label1" AssociatedControlID="txtNomeUser" runat="server" Text="">Nome</asp:Label>
            <asp:TextBox class="form-control" ID="txtNomeUser" Text="" runat="server"></asp:TextBox>
        </div>
        <asp:RequiredFieldValidator CssClass="text-danger" ID="RequiredFieldValidator2" runat="server" ErrorMessage="Campo obrigatório" ControlToValidate="txtNomeUser"></asp:RequiredFieldValidator>
        <br />

        <div class="form-group">
            <asp:Label ID="Label2" AssociatedControlID="lblEmailUser" runat="server" Text="Label">Email</asp:Label>
            <asp:Label ID="lblEmailUser" runat="server" Text=""></asp:Label>
            <%--<asp:TextBox class="form-control" ID="txtEmailUser" Text="" runat="server"></asp:TextBox>--%>
        </div>
        <%--<asp:RequiredFieldValidator CssClass="text-danger" ID="RequiredFieldValidator3" runat="server" ErrorMessage="Campo obrigatório" ControlToValidate="txtEmailUser"></asp:RequiredFieldValidator>--%>
        
        <div class="form-group">
            <asp:Label ID="Label3" AssociatedControlID="cbOficial" runat="server" Text="">Oficial</asp:Label>
            <asp:CheckBox class="form-control" ID="cbOficial" runat="server" />
            <%--<asp:TextBox class="form-control" ID="TextBox1" Text="" runat="server"></asp:TextBox>--%>
        </div>
        <%--<asp:RequiredFieldValidator CssClass="text-danger" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Campo obrigatório" ControlToValidate="txtNomeUser"></asp:RequiredFieldValidator>--%>
                
        <asp:Button class="form-control" ID="btnSalvar" runat="server" Text="Salvar" OnClick="btnSalvar_Click" />

    </div>

</asp:Content>
