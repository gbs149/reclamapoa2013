﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using ReclamaPoa2013.Models;

namespace ReclamaPoa2013.admin
{
    public partial class usuarios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        public IQueryable<ReclamaPoa2013.Models.UserViewModel> listaUsuarios_GetData()
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();

            IQueryable<UserViewModel> query = from u in manager.Users
                                              select new UserViewModel
                                              {
                                                  Id = u.Id,
                                                  Nome = u.Nome,
                                                  Email = u.Email,
                                                  Oficial = u.Oficial.ToString()
                                              };
            return query;
        }


    }
}