﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="categorias.aspx.cs" Inherits="ReclamaPoa2013.admin.categorias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div style="min-height: 20px"></div>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th></th>
                <th>ID</th>
                <th>Nome</th>
                <th>Descrição</th>
            </tr>
        </thead>
        <tbody>
            <asp:ListView
                ID="ListView1"
                ItemType="ReclamaPoa2013.Models.Categoria"
                runat="server"
                SelectMethod="getCategorias">
                <ItemTemplate>
                    <tr>
                        <td>
                            <a href="editar-categoria.aspx?id=<%#:Item.CategoriaId %>" class="btn btn-sm btn-link">Editar</a>
                        </td>
                        <td>
                            <%#:Item.CategoriaId %>
                        </td>
                        <td>
                            <%#:Item.Nome %>
                        </td>
                        <td>
                            <%#:Item.Descricao %>
                        </td>

                    </tr>
                </ItemTemplate>
            </asp:ListView>
            
        </tbody>
    </table>
    <a href="nova-categoria.aspx" class="btn btn-success">Nova Categoria</a>

</asp:Content>
