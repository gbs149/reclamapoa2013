﻿using ReclamaPoa2013.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ReclamaPoa2013.admin
{
    public partial class editar_categoria : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                ReclamaPoaEntities _db = new ReclamaPoaEntities();
                int id;

                Categoria categoria = new Categoria();

                if (int.TryParse(Request.QueryString["id"], out id))
                {
                    var query = _db.Categorias.Where(c => c.CategoriaId == id);
                    categoria = query.First();
                }

                txtNomeCat.Text = categoria.Nome;
                txtDescCat.Text = categoria.Descricao;
                lblCategoriaId.Text = categoria.CategoriaId.ToString();
            }
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {

            int id;

            if (!int.TryParse(lblCategoriaId.Text, out id))
            {
                Response.Redirect("Default");
            }

            ReclamaPoaEntities _db = new ReclamaPoaEntities();

            Categoria categoria = (from c in _db.Categorias
                                   where c.CategoriaId == id
                                   select c).First();

            if (categoria == null)
                Response.Redirect("Default");
            else
            {
                categoria.Nome = txtNomeCat.Text;
                categoria.Descricao = txtDescCat.Text;

                _db.SaveChanges();
                Response.Redirect("categorias");
            }
        }
    }
}