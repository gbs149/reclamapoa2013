﻿using ReclamaPoa2013.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ReclamaPoa2013.admin
{
    public partial class nova_categoria : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            ReclamaPoaEntities _db = new ReclamaPoaEntities();
            Categoria novaCategoria = new Categoria();

            novaCategoria.Nome = txtNomeCat.Text;
            novaCategoria.Descricao = txtDescCat.Text;

            _db.Categorias.Add(novaCategoria);
            _db.SaveChanges();

            Response.Redirect("categorias");
           

        }
    }
}