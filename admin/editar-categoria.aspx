﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="editar-categoria.aspx.cs" Inherits="ReclamaPoa2013.admin.editar_categoria" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div>

        <asp:Label ID="lblCategoriaId" runat="server" Text=""></asp:Label>
        <div class="form-group">
            <asp:Label ID="Label1" AssociatedControlID="txtNomeCat" runat="server" Text="">Título da Categoria</asp:Label>
            <asp:TextBox class="form-control" ID="txtNomeCat" Text="" runat="server"></asp:TextBox>
        </div>
        <asp:RequiredFieldValidator CssClass="text-danger" ID="RequiredFieldValidator2" runat="server" ErrorMessage="Campo obrigatório" ControlToValidate="txtNomeCat"></asp:RequiredFieldValidator>
        <br />

        <div class="form-group">
            <asp:Label ID="Label2" AssociatedControlID="txtDescCat" runat="server" Text="Label">Descrição</asp:Label>
            <asp:TextBox class="form-control" ID="txtDescCat" TextMode="MultiLine" Rows="5" Columns="60" Text="" runat="server"></asp:TextBox>
        </div>
        <asp:RequiredFieldValidator CssClass="text-danger" ID="RequiredFieldValidator3" runat="server" ErrorMessage="Campo obrigatório" ControlToValidate="txtDescCat"></asp:RequiredFieldValidator>
                
        <asp:Button class="form-control" ID="btnSalvar" runat="server" Text="Salvar" OnClick="btnSalvar_Click" />

    </div>
</asp:Content>
