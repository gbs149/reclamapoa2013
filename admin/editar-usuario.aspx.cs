﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace ReclamaPoa2013.admin
{
    public partial class editar_usuario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var userEmail = Request.QueryString["email"];
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var userEdit = manager.FindByEmail(userEmail);

                txtNomeUser.Text = userEdit.Nome;
                lblEmailUser.Text = userEdit.Email;
                cbOficial.Checked = userEdit.Oficial;
                
            }

        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var userEdit = manager.FindByEmail(lblEmailUser.Text);

            userEdit.Nome = txtNomeUser.Text;
            userEdit.Oficial = cbOficial.Checked;

            manager.Update(userEdit);

            Response.Redirect("usuarios");
        }
    }
}