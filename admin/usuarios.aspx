﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="usuarios.aspx.cs" Inherits="ReclamaPoa2013.admin.usuarios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div style="min-height: 20px"></div>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th></th>
                <th>Nome</th>
                <th>Email</th>
                <th>Oficial</th>
            </tr>
        </thead>
        <tbody>
            <asp:ListView id="listaUsuarios" runat="server" ItemType="ReclamaPoa2013.Models.UserViewModel" SelectMethod="listaUsuarios_GetData">
                <ItemTemplate>
                    <tr>
                        <td><a href="editar-usuario.aspx?email=<%#:Item.Email %>" class="btn btn-sm btn-link">Editar</a></td>
                        <td><%#:Item.Nome %></td>
                        <td><%#:Item.Email %></td>
                        <td><%#:Item.Oficial %></td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </tbody>
    </table>
</asp:Content>
