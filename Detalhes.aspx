﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Detalhes.aspx.cs" Inherits="ReclamaPoa2013.Detalhes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>

    <div class="jumbotron">
        <asp:FormView
            ID="detalhesReclamacao"
            runat="server"
            ItemType="ReclamaPoa2013.Models.ReclamacaoViewModel"
            SelectMethod="getReclamacao">
            <ItemTemplate>
                
                    <h1><%#:Item.Titulo%></h1>

                    <asp:Button CssClass="btn btn-warning" Visible="false" ID="btnEditar" runat="server" Text="Editar" OnClick="btnEditar_Click" />
                    <asp:Button CssClass="btn btn-success" Visible="false" ID="btnResolvida" runat="server" Text="Marcar como resolvida" OnClick="btnResolvida_Click" />
                    <asp:Button CssClass="btn btn-danger" Visible="false" ID="btnEncerra" runat="server" Text="Encerrar Reclamação" OnClick="btnEncerra_Click" />


                    <p>Data da reclamação: <%#:Item.Data.ToShortDateString()%></p>
                    <p><span class="label label-default"><%#:Item.Categoria%></span></p>
                    <p>Bairro: <%#:Item.Bairro%></p>
                    <p>Registrada por: <em><%#:Item.Usuario%></em></p>
                    <p><span class="label label-warning"><%#:Item.Situacao%></span></p>
                    <p><%#:Item.Descricao%></p>
                    <img style="max-width:800px" class="img-responsive" src="<%#:Item.UrlImagem%>" />

                

            </ItemTemplate>
        </asp:FormView>


        <asp:LoginView Visible="true" ID="LoginView1" runat="server" ViewStateMode="Disabled">
            <LoggedInTemplate>
                <h2>Participe da Discussão</h2>
                <asp:TextBox CssClass="form-control" ID="txtComentario"
                    TextMode="MultiLine"
                    Columns="50"
                    Rows="5"
                    runat="server" />
                <asp:Button CssClass="form-control" ID="btnComentario" runat="server" Text="Enviar comentário" OnClick="btnComentario_Click" />
            </LoggedInTemplate>
        </asp:LoginView>


        <div class="panel-group">
            <asp:ListView
                ID="lvComentarios"
                ItemType="ReclamaPoa2013.Models.ComentarioViewModel"
                SelectMethod="getComentarios"
                runat="server">
                <ItemTemplate>
                    <div class="panel panel-default">
                        <div class="panel-heading"><%#:Item.Usuario %></div>
                        <div class="panel-body"><%#:Item.Texto %></div>
                    </div>
                </ItemTemplate>
            </asp:ListView>
        </div>

    </div>


</asp:Content>
