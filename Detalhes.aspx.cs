﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReclamaPoa2013.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;


namespace ReclamaPoa2013
{
    public partial class Detalhes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String filtro = (string)Request.QueryString["id"];
            int id;

            // altera a visibilidade dos botões editar e encerrar reclamação
            if (int.TryParse(filtro, out id))
            {
                ReclamaPoaEntities _db = new ReclamaPoaEntities();
                Reclamacao rec = _db.Reclamacoes.Where(r => r.ReclamacaoId == id).First();
                Button btnEditar = (Button)detalhesReclamacao.FindControl("btnEditar");
                Button btnEncerra = (Button)detalhesReclamacao.FindControl("btnEncerra");
                Button btnResolvida = (Button)detalhesReclamacao.FindControl("btnResolvida");

                var currentUserId = User.Identity.GetUserId();
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var currentUser = manager.FindById(currentUserId);

                if (User.Identity.IsAuthenticated)
                {
                    if (currentUser.UserName == rec.Usuario && !(btnEditar == null))
                    {
                        btnEditar.Visible = true;
                        if(rec.Situacao != Situacao.Resolvida)
                        btnResolvida.Visible = true;
                    }

                    if (currentUser.Oficial && rec.Situacao == Situacao.Aberta)
                    {
                        btnEncerra.Visible = true;
                    }
                }

                // desabilita comentarios para reclamações encerradas
                if (rec.Situacao == Situacao.Encerrada)
                {
                    LoginView1.Visible = false;
                }
            }
        }


        /// <summary>
        /// Recupera a reclamação a partir do ID e o nome do usuario a partir do email
        /// </summary>
        /// <returns></returns>
        public ReclamacaoViewModel getReclamacao()
        {
            String filtro = (string)Request.QueryString["id"];
            int id;

            if (filtro != null && Int32.TryParse(filtro, out id))
            {
                ReclamaPoaEntities _db = new ReclamaPoaEntities();

                var query = from reclamacao in _db.Reclamacoes
                            where reclamacao.ReclamacaoId == id
                            select new ReclamacaoViewModel()
                            {
                                Titulo = reclamacao.Titulo,
                                Descricao = reclamacao.Descricao,
                                Data = reclamacao.Data,
                                Endereco = reclamacao.Endereco,
                                Situacao = reclamacao.Situacao.ToString(),
                                Bairro = reclamacao.Bairro.Nome,
                                Categoria = reclamacao.Categoria.Nome,
                                UrlImagem = reclamacao.UrlImagem,
                                Usuario = reclamacao.Usuario
                            };

                var rvm = query.First();

                string emailUsuario = rvm.Usuario;

                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var nomeUsuario = manager.FindByEmail(emailUsuario).Nome;

                rvm.Usuario = nomeUsuario;

                return rvm;

            }
            else return null;
        }



        /// <summary>
        /// Recupera os comentarios da reclamacao
        /// </summary>
        /// <returns></returns>
        public IQueryable<ComentarioViewModel> getComentarios()
        {
            int id;

            if (Int32.TryParse(Request.QueryString["id"], out id))
            {
                ReclamaPoaEntities _db = new ReclamaPoaEntities();

                IQueryable<ComentarioViewModel> cvmData = from c in _db.Comentarios
                                                          where c.Reclamacao.ReclamacaoId == id
                                                          select new ComentarioViewModel
                                                          {
                                                              ComentarioId = c.ComentarioId,
                                                              Texto = c.Texto,
                                                              Usuario = c.Usuario,
                                                              ReclamacaoId = c.Reclamacao.ReclamacaoId
                                                          };
                return cvmData;
            }
            else return null;
        }



        protected void btnComentario_Click(object sender, EventArgs e)
        {
            ReclamaPoaEntities _db = new ReclamaPoaEntities();
            Comentario comentario = new Comentario();
            TextBox textoComentario = (TextBox)LoginView1.FindControl("txtComentario");

            String filtro = (string)Request.QueryString["id"];
            int id;
            if (Int32.TryParse(filtro, out id))
            {
                // query para pegar a reclamação
                var query = _db.Reclamacoes.Where(r => r.ReclamacaoId == id);
                Reclamacao r1 = query.First();

                comentario.Texto = textoComentario.Text;
                comentario.Reclamacao = r1;
                comentario.Usuario = Context.User.Identity.Name;
            }
            _db.Comentarios.Add(comentario);
            _db.SaveChanges();

            Response.Redirect("Detalhes.aspx?id=" + id);
        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            String filtro = (string)Request.QueryString["id"];
            int id;

            if (Int32.TryParse(filtro, out id))
            {
                Response.Redirect("editar-reclamacao?id=" + id);
            }

        }

        protected void btnEncerra_Click(object sender, EventArgs e)
        {

            String filtro = (string)Request.QueryString["id"];
            int id;

            if (Int32.TryParse(filtro, out id))
            {
                ReclamaPoaEntities _db = new ReclamaPoaEntities();

                Reclamacao rec = _db.Reclamacoes.Where(r => r.ReclamacaoId == id).First();
                rec.Situacao = Situacao.Encerrada;

                _db.SaveChanges();

                Response.Redirect("Detalhes.aspx?id=" + id);

            }

        }

        protected void btnResolvida_Click(object sender, EventArgs e)
        {
            String filtro = (string)Request.QueryString["id"];
            int id;

            if (Int32.TryParse(filtro, out id))
            {
                ReclamaPoaEntities _db = new ReclamaPoaEntities();

                Reclamacao rec = _db.Reclamacoes.Where(r => r.ReclamacaoId == id).First();
                rec.Situacao = Situacao.Resolvida;

                _db.SaveChanges();

                Response.Redirect("Detalhes.aspx?id=" + id);

            }
        }
    }
}