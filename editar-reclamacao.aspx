﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="editar-reclamacao.aspx.cs" Inherits="ReclamaPoa2013.editar_reclamacao" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <div class="form-group">
            <asp:Label ID="Label1" AssociatedControlID="txtNomeRec" runat="server" Text="Label">Título da Reclamação</asp:Label>
            <asp:TextBox class="form-control" ID="txtNomeRec" runat="server"></asp:TextBox>
        </div>
        <asp:RequiredFieldValidator CssClass="text-danger" ID="RequiredFieldValidator2" runat="server" ErrorMessage="Campo obrigatório" ControlToValidate="txtNomeRec"></asp:RequiredFieldValidator>
        <br />

        <div class="form-group">
            <asp:Label ID="Label3" runat="server" AssociatedControlID="cbResolvida" Text="Marcar como resolvida?"></asp:Label>
            <asp:CheckBox ID="cbResolvida" runat="server" />
        </div>

        <div class="form-group">
            <asp:Label ID="Label8" runat="server" AssociatedControlID="lblData" Text="data: "></asp:Label>
            <asp:Label ID="lblData" runat="server" Text=""></asp:Label>
        </div>

        <div class="form-group">
            <asp:Label ID="Label5" runat="server" AssociatedControlID="lblBairro" Text="Bairro: "></asp:Label>
            <asp:Label ID="lblBairro" runat="server" Text=""></asp:Label>
        </div>

        <div class="form-group">
            <asp:Label ID="Label7" runat="server" AssociatedControlID="lblCategoria" Text="Categoria: "></asp:Label>
            <asp:Label ID="lblCategoria" runat="server" Text=""></asp:Label>
        </div>

        <div class="form-group">
            <asp:Label ID="Label2" AssociatedControlID="txtDescRec" runat="server" Text="Label">Descrição</asp:Label>
            <asp:TextBox class="form-control" ID="txtDescRec" TextMode="MultiLine" Rows="5" Columns="60" runat="server"></asp:TextBox>
        </div>
        <asp:RequiredFieldValidator CssClass="text-danger" ID="RequiredFieldValidator3" runat="server" ErrorMessage="Campo obrigatório" ControlToValidate="txtDescRec"></asp:RequiredFieldValidator>


        <div class="form-group">
            <asp:Label ID="Label4" AssociatedControlID="txtEndereco" runat="server" Text="Label">Endereço</asp:Label>
            <asp:TextBox class="form-control" ID="txtEndereco" runat="server"></asp:TextBox>
        </div>
        <asp:RequiredFieldValidator CssClass="text-danger" ID="RequiredFieldValidator5" runat="server" ErrorMessage="Campo obrigatório" ControlToValidate="txtEndereco"></asp:RequiredFieldValidator>

        <asp:Image ImageUrl="images/placeholder.png" ID="img1" runat="server" />
        <div class="form-group">
            <asp:Label ID="Label6" AssociatedControlID="txtUrlImagem" runat="server" Text="Label">Link para imagem</asp:Label>
            <asp:TextBox class="form-control" ID="txtUrlImagem" runat="server"></asp:TextBox>
        </div>



        <asp:Button class="form-control" ID="btnSalvar" runat="server" Text="Salvar" OnClick="btnSalvar_Click"/>

    </div>
</asp:Content>
