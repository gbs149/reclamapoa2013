﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Reclamacoes.aspx.cs" Inherits="ReclamaPoa2013.Reclamacoes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div style="min-height: 20px"></div>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <div class="form-group col-sm-2">
                <asp:Label AssociatedControlID="ddlBairros" ID="Label1" runat="server" Text="">Bairros</asp:Label>
                <asp:DropDownList CssClass="form-control" ID="ddlBairros" AutoPostBack="true" OnSelectedIndexChanged="ddlBairros_SelectedIndexChanged" runat="server"></asp:DropDownList>

                <asp:Label AssociatedControlID="ddlCategorias" ID="Label2" runat="server" Text="">Categorias</asp:Label>
                <asp:DropDownList CssClass="form-control" ID="ddlCategorias" AutoPostBack="true" OnSelectedIndexChanged="ddlCategorias_SelectedIndexChanged" runat="server"></asp:DropDownList>

                <asp:Label AssociatedControlID="ddlSituacao" Font-Bold="true" ID="Label5" runat="server" Text="">Situação</asp:Label>
                <asp:DropDownList CssClass="form-control" ID="ddlSituacao" AutoPostBack="true" OnSelectedIndexChanged="ddlSituacao_SelectedIndexChanged" runat="server"></asp:DropDownList>

                <%--<br />
                <br />
                <asp:Label ID="Label6" runat="server" Text="Perído:">Período: </asp:Label>
                <br />
                <asp:Label AssociatedControlID="txtDataInicio" ID="Label3" runat="server" Text="">Data inicial</asp:Label>
                <asp:TextBox CssClass="form-control" ID="txtDataInicio" AutoPostBack="false" runat="server"></asp:TextBox>
                <ajaxToolkit:CalendarExtender TargetControlID="txtDataInicio" Format="dd/MM/yyyy" ID="CalendarExtender1" runat="server" />

                <asp:Label AssociatedControlID="txtDataFinal" ID="Label4" runat="server" Text="">Datafinal</asp:Label>
                <asp:TextBox CssClass="form-control" ID="txtDataFinal" AutoPostBack="false" runat="server"></asp:TextBox>
                <ajaxToolkit:CalendarExtender TargetControlID="txtDataFinal" Format="dd/MM/yyyy" ID="CalendarExtender2" runat="server" />

                <asp:Button CssClass="btn btn-default btn-block" ID="btnPesquisaData" runat="server" Text="Pesquisar por data" OnClick="btnPesquisaData_Click" />--%>
                
            </div>


            <div class="col-sm-10">

                <asp:ListView
                    ID="lvReclamacoes"
                    runat="server"
                    ItemType="ReclamaPoa2013.Models.ReclamacaoViewModel"
                    SelectMethod="getReclamacoes">
                    <EmptyDataTemplate>
                        <h1>Nenhuma reclamação encontrada</h1>
                    </EmptyDataTemplate>

                    <ItemTemplate>
                        <div class="col-sm-4" style="height: 45rem">
                            <div style="height: auto">
                                <a href="Detalhes.aspx?id=<%#:Item.ReclamacaoId %>">
                                    <h2><%#:Item.Titulo %></h2>
                                </a>
                            </div>
                            <br />
                            <img class="img-responsive" src="<%#: Item.UrlImagem %>" />
                            <p>Registrada em: <%#:Item.Data.ToShortDateString() %></p>

                            <p>Bairro: <%#:Item.Bairro %></p>
                            <span class="label label-default"><%#:Item.Categoria %></span>
                            <span class="label label-warning"><%#:Item.Situacao %></span>
                            <br />
                        </div>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
