﻿using ReclamaPoa2013.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ReclamaPoa2013
{
    public partial class Reclamacoes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ReclamaPoaEntities _db = new ReclamaPoaEntities();

                ddlBairros.DataSource = _db.Bairros.ToList();
                ddlBairros.DataTextField = "Nome";
                ddlBairros.DataValueField = "BairroId";
                ddlBairros.DataBind();

                ddlBairros.Items.Insert(0, new ListItem("Todos", "0"));
                ddlBairros.SelectedIndex = 0;

                ddlCategorias.DataSource = _db.Categorias.ToList();
                ddlCategorias.DataTextField = "Nome";
                ddlCategorias.DataValueField = "CategoriaId";
                ddlCategorias.DataBind();

                ddlCategorias.Items.Insert(0, new ListItem("Todas", "0"));
                ddlCategorias.SelectedIndex = 0;

                ddlSituacao.Items.Insert(0, new ListItem("Todas", "0"));
                ddlSituacao.Items.Insert(1, new ListItem("Abertas", "1"));
                ddlSituacao.Items.Insert(2, new ListItem("Resolvidas", "2"));
                ddlSituacao.Items.Insert(3, new ListItem("Encerradas", "3"));
                ddlSituacao.SelectedIndex = 0;

            }
        }



        public IQueryable<ReclamacaoViewModel> GetReclamacoes()
        {
            ReclamaPoaEntities _db = new ReclamaPoaEntities();

            IQueryable<ReclamacaoViewModel> query = from r in _db.Reclamacoes
                                                    orderby r.Data descending
                                                    select new ReclamacaoViewModel
                                                    {
                                                        ReclamacaoId = r.ReclamacaoId,
                                                        Titulo = r.Titulo,
                                                        Descricao = r.Descricao,
                                                        Data = r.Data,
                                                        Endereco = r.Endereco,
                                                        Situacao = r.Situacao.ToString(),
                                                        Bairro = r.Bairro.Nome,
                                                        Categoria = r.Categoria.Nome,
                                                        UrlImagem = r.UrlImagem
                                                    };


            int categoriaId = int.Parse(ddlCategorias.SelectedValue);
            int bairroId = int.Parse(ddlBairros.SelectedValue);
            int situacao = int.Parse(ddlSituacao.SelectedValue);

            String txtSituação;

            //String txtInicio = txtDataInicio.Text;
            //String txtFim = txtDataFinal.Text;
            //DateTime dtInicio = new DateTime();
            //DateTime dtFim = new DateTime();

            if (situacao == 1) txtSituação = "Aberta";
            else if (situacao == 2) txtSituação = "Resolvida";
            else if (situacao == 3) txtSituação = "Encerrada";
            else txtSituação = "Todas";

            
            
            // pesquisa só por bairro
            if (bairroId != 0 && categoriaId == 0 && situacao == 0)
            {
                Bairro bairro = _db.Bairros.Where(b => b.BairroId == bairroId).First();
                query = query.Where(r => r.Bairro == bairro.Nome);
                lvReclamacoes.DataBind();
                return query;
            }

            // pesquisa só por categoria
            else if (bairroId == 0 && categoriaId != 0 && situacao == 0)
            {
                Categoria categoria = _db.Categorias.Where(c => c.CategoriaId == categoriaId).First();
                query = query.Where(r => r.Categoria == categoria.Nome);
                lvReclamacoes.DataBind();
                return query;
            }

            // pesquisa só por situação
            else if (bairroId == 0 && categoriaId == 0 && situacao != 0)
            {
                query = query.Where(r => r.Situacao == txtSituação);
                lvReclamacoes.DataBind();
                return query;
            }

            // pesquisa por bairro e categoria
            else if (bairroId != 0 && categoriaId != 0 && situacao == 0)
            {
                Bairro bairro = _db.Bairros.Where(b => b.BairroId == bairroId).First();
                Categoria categoria = _db.Categorias.Where(c => c.CategoriaId == categoriaId).First();
                query = query.Where(r => r.Bairro == bairro.Nome && r.Categoria == categoria.Nome);
                lvReclamacoes.DataBind();
                return query;
            }

            // pesquisa por bairro e situação
            else if (bairroId != 0 && categoriaId == 0 && situacao != 0)
            {
                Bairro bairro = _db.Bairros.Where(b => b.BairroId == bairroId).First();
                query = query.Where(r => r.Bairro == bairro.Nome && r.Situacao == txtSituação);
                lvReclamacoes.DataBind();
                return query;
            }

            // pesquisa por categoria e situação
            else if (bairroId == 0 && categoriaId != 0 && situacao != 0)
            {
                Categoria categoria = _db.Categorias.Where(c => c.CategoriaId == categoriaId).First();
                query = query.Where(r => r.Categoria == categoria.Nome && r.Situacao == txtSituação);
                lvReclamacoes.DataBind();
                return query;
            }

            // pesquisa por bairro, categoria e situação
            else if (bairroId != 0 && categoriaId != 0 && situacao != 0)
            {
                Bairro bairro = _db.Bairros.Where(b => b.BairroId == bairroId).First();
                Categoria categoria = _db.Categorias.Where(c => c.CategoriaId == categoriaId).First();
                query = query.Where(r => r.Bairro == bairro.Nome && r.Categoria == categoria.Nome && r.Situacao == txtSituação);
                lvReclamacoes.DataBind();
                return query;
            }

            else return query;
        }



        protected void ddlCategorias_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetReclamacoes();
        }

        protected void ddlBairros_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetReclamacoes();
        }

        protected void ddlSituacao_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetReclamacoes();
        }
    }
}